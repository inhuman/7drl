REBAR=$(shell which rebar || echo ./rebar)
REBAR_URL=https://github.com/rebar/rebar/wiki/rebar
.PHONY: all test deps clean

all: $(REBAR) deps compile start

start:
	./start.sh

app: $(REBAR)
	mkdir -p apps/${name}
	cd apps/${name} && ../../rebar create-app appid=${name}

compile: $(REBAR)
	$(REBAR) compile

deps: $(REBAR)
	$(REBAR) get-deps

clean: $(REBAR) testclean
	$(REBAR) clean

distclean: $(REBAR) clean relclean
	$(REBAR) delete-deps
	rm -f ./rebar

TEST_LOG_FILE := eunit.log
testclean:
	@rm -f $(TEST_LOG_FILE)

# Test each dependency individually in its own VM
test: $(REBAR) deps compile testclean
	# @$(foreach dep, \
 #            $(wildcard deps/*), \
 #               (cd $(dep) && ../../rebar eunit deps_dir=.. skip_deps=true)  \
 #               || echo "Eunit: $(notdir $(dep)) FAILED" >> $(TEST_LOG_FILE);)
	$(REBAR) eunit skip_deps=true
	@if test -s $(TEST_LOG_FILE) ; then \
						 cat $(TEST_LOG_FILE) && \
						 exit `wc -l < $(TEST_LOG_FILE)`; \
				fi

##
## Release targets
##
rel: $(REBAR) deps
	$(REBAR) compile generate

relclean:
	rm -rf rel/eggs

./rebar:
	erl -noshell -s inets -s ssl \
		-eval 'httpc:request(get, {"$(REBAR_URL)", []}, [], [{stream, "./rebar"}])' \
		-s init stop
	chmod +x ./rebar