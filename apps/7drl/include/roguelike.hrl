-type location() :: {number(), number()}.
-type entity() :: {atom, pid()}.

-record (action, {
  type :: binary(),
  by :: entity(),
  at :: location(),
  to :: location()
}).

-type action() :: #action{}.

-record (tile, {
  x,
  y,
  w,
  h,
  entities = [],
  walkable=true
}).

-record (activation, {
  by :: entity(),
  map :: pid(),
  xp :: number(),
  at :: location(),
  to :: location()
}).

-record (effect, {
  name, 
  duration = 1, 
  x, 
  y, 
  r = 0,
  action = undefined
}).

-define (UP_ARROW, 38).
-define (DOWN_ARROW, 40).
-define (LEFT_ARROW, 37).
-define (RIGHT_ARROW, 39).
-define (LEFT_MOUSE, 0).
-define (MIDDLE_MOUSE, 1).
-define (RIGHT_MOUSE, 2).

-define (POSSIBLE_RUNES,
  [<<"a">>,<<"b">>,<<"c">>,<<"d">>,<<"e">>,<<"f">>,
  <<"g">>,<<"h">>,<<"i">>,<<"j">>,<<"k">>,<<"l">>,
  <<"m">>,<<"n">>,<<"o">>,<<"p">>,<<"q">>,<<"r">>,
  <<"s">>,<<"t">>,<<"u">>,<<"v">>,<<"w">>,<<"x">>,
  <<"y">>,<<"z">>,
  <<"A">>,<<"B">>,<<"C">>,<<"D">>,<<"E">>,<<"F">>,
  <<"G">>,<<"H">>,<<"I">>,<<"J">>,<<"K">>,<<"L">>,
  <<"M">>,<<"N">>,<<"O">>,<<"P">>,<<"Q">>,<<"R">>,
  <<"S">>,<<"T">>,<<"U">>,<<"V">>,<<"W">>,<<"X">>,
  <<"Y">>,<<"Z">>]
).