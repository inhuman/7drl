Crafty.c "Camera",
  init: ->

  follow: (entity) ->
    @updateCamera entity.x, entity.y
    entity.bind "Move", =>
      @updateCamera entity.x, entity.y

  updateCamera: (x, y) ->
      Crafty.viewport.x = -x + Crafty.viewport.width / 2;
      Crafty.viewport.y = -y + Crafty.viewport.height / 2;