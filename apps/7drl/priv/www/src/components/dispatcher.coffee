Crafty.c "Dispatcher",

  init: ->
    @entities = {}

  connect: (ws) ->
    @ws = ws

    @ws.onopen = (e) ->
      console.log "Connected"

    @ws.onclose = (e) ->

    @ws.onmessage = ({data}) =>
      [event, payload] = JSON.parse data

      for {id, data} in payload
        do =>
          switch id
            when "Player", "Map"
              @entities[id]?.trigger "Update", data

    @ws.onerror = (e) ->

    @

  send: (event, payload) ->
    if (@ws.readyState < 1)
      @ws.onopen = =>
        @send event, payload
    else
      @ws.send JSON.stringify [event, payload]