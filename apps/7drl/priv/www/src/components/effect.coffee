Crafty.c "Effect",
  init: ->
    @requires "Rogue"

    @lastStart = null

    @bind "EnterFrame", ->
      return unless @lastStart?
      currentTime = new Date().getTime()
      if currentTime - @lastStart > 150
        @text ""
        @textColor "#000000"
        @lastStart = null

    @bind "Update", ([type, value]) ->
      @text "." if @text() isnt "."
      @lastStart = new Date().getTime()