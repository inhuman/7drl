Crafty.c "Map",

  init: ->
    @requires "2D,DOM, Mouse"
    @tileWidth = 16
    @tileHeight = 16
    @tiles = {}
    @effects = {}

    @camera = Crafty.e("Camera")
    Crafty.viewport.width = 800
    Crafty.viewport.height = 600

    @bind "Update", ({x,y,r,symbols}) ->
      if symbols is "none"
        @trigger "GameOver", {x:@x, y:@y}
        return

      @x = x-r unless @x is x-r
      @y = y-r unless @y is y-r
      @w = 2*r unless @w is 2*r
      @h = 2*r unless @h is 2*r

      if r != @r
        for j in [@y..@y+2*@r] by @tileWidth
          for i in [@x..@x+2*@r] by @tileHeight
            index = "#{i-@x},#{j-@y}"
            if @tiles[index]?
              @tiles[index].destroy()
              @tiles[index] = undefined
        @r = r

      for j in [@y..@y+2*r] by @tileWidth
        for i in [@x..@x+2*r] by @tileHeight
          index = "#{i-@x},#{j-@y}"
          isPlayer = i is x and j is y
          symbol = symbols["#{i},#{j}"]
          [type, value] = symbol ? ["symbol","."]
          # Remove in-view symbols after handling.
          delete symbols["#{i},#{j}"] if symbol?

          entity = @tiles[index]
          unless entity?
            entity = Crafty.e("Rogue").attr
              x: i
              y: j
              w: @tile_width
              h: @tile_height

            if isPlayer
              entity.addComponent "Player"
              @camera.follow entity

            @tiles[index] = entity
            @attach entity

            entity.bind "MouseOver", (e) ->
              entitySymbol = entity.text()
              console.log entitySymbol

          # When the player has rune, the center symbol will be
          # the symbol of the rune, so we let others handle it
          # and reset back to "@" because it's the player.
          if isPlayer
            if value isnt "@"
              @trigger "PlayerStatus", {x:@x, y:@y,data:value}
              value = "@"
            else
              @trigger "PlayerStatus", {x:@x, y:@y,data:"none"}

          entity.trigger "Update", [type, value]

      # Display the effects somewhere else.
      for own coordsKey, [type, value] of symbols
        if type is "effect" and value is "shot"
          [i, j] = coordsKey.split(",")
          effect = @effects[coordsKey]
          unless effect?
            effect = Crafty.e("Rogue, Effect").attr
              x: i
              y: j
              w: @tileWidth
              h: @tileHeight

            @effects[coordsKey] = effect

          effect.trigger "Update", [type, value]


    @bind "MouseMove", ({clientX, clientY}) ->
      {x, y} = Crafty.DOM.translate clientX, clientY
      {atan2, abs, PI} = Math

      # angle = atan2(@y+@h/2-y, @x+@w/2-x) - atan2(@h/2, @w/2)
      # angle = if abs(angle) < 2*PI - abs(angle) then angle else 2*PI - abs(angle)
      # angle = angle - PI/4
      # console.log "angle", angle
      # @rotation = Crafty.math.radToDeg angle
