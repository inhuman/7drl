Crafty.c "Rogue",
  init: ->
    @requires "2D, DOM, Text, Mouse"

    @textFont
      family: "Droid Sans Mono, monospace"
    @textColor '#FF0000'

    @colors = {
        "shot": '#F1FF32'
        "pit": '#706F73'
        "fire": '#FFBB00'
        "smoke": '#4A4A4A'
        "portal": '#66ff00'
        "#": '#663300'
        ".": '#FF0000'
        "@": '#FF0000'
    }

    @bind "Update", ([type, value]) ->
      if type is "effect"
        matches = value.match(/([!0#$%^&*]?)([a-z]+)/)
        if matches?
          if matches[1] != ""
            @text matches[1]
          if matches[2] != ""
            @textColor @colors[matches[2]]

      if type is "symbol"
        unless @text() is value
          @text value
        {_textColor, _strength} = @
        if value == "#"
          unless _textColor is Crafty.toRGB(@colors[value], _strength)
            @textColor @colors[value]
        else
          unless _textColor is Crafty.toRGB('#FF0000', _strength)
            @textColor '#FF0000'
