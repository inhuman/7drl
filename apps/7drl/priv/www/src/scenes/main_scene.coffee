{UP_ARROW, DOWN_ARROW, LEFT_ARROW, RIGHT_ARROW, W, S, A, D} = Crafty.keys
{LEFT, MIDDLE, RIGHT} = Crafty.mouseButtons

Crafty.scene "Main", ->

  wsUrl = 'ws://localhost:8008/game'
  ws = new WebSocket wsUrl

  dispatcher = Crafty.e("Dispatcher")
  dispatcher.connect(ws)

  Crafty.background("black")

  hud = Crafty.e("2D, DOM, Text").attr
              x: Crafty.DOM.window.width - 150
              y: 100
              w: 85
            .textColor("#FF00000")
            .text "Rune: N/A\nHP: N/A\nDmg: N/A"

  map = Crafty.e("Map")
  dispatcher.entities["Map"] = map

  map.bind "PlayerStatus", ({x, y, data}) ->
    [symbol, hp, damage] = data.split(",")
    symbol = "N/A" if symbol is "@"
    statusText = "Rune: #{symbol} \n HP: #{hp} \n Dmg: #{damage}"
    hud.attr({x: x+Crafty.viewport.width/2-150, y:y})
    hud.text statusText unless hud.text() is statusText

  map.bind "GameOver", ({x, y}) ->
    msgBoxDimension = w: 400, h: 300
    box = Crafty.e("2D, DOM, Text").attr
      x: Crafty.viewport.width/2 - 200
      y: Crafty.viewport.height/2 - 150
      w: 400
      h: 300
    box.text("You are dead. Respawn in 5 seconds")
    box.textColor "#FF00000"

  map.bind "KeyDown", ({key}) ->
    switch key
      when UP_ARROW, DOWN_ARROW, LEFT_ARROW, RIGHT_ARROW
        dispatcher.send "Input", key
      when W, S, A, D
        key = UP_ARROW if key is W
        key = DOWN_ARROW if key is S
        key = LEFT_ARROW if key is A
        key = RIGHT_ARROW if key is D
        dispatcher.send "Input", key

  Crafty.addEvent @, Crafty.stage.elem, "mousedown", ({which, button, clientX, clientY}) ->
    {x, y} = Crafty.DOM.translate clientX, clientY
    # Normalize button according to http://unixpapa.com/js/mouse.html
    mouseButton = null
    unless which?
      mouseButton = if button < 2 then LEFT else if button is 4 then MIDDLE else RIGHT
    else
      mouseButton = if which < 2 then LEFT else if which is 2 then MIDDLE else RIGHT

    dispatcher.send "Input", [mouseButton, x, y]

