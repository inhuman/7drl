-module('7drl_app').

-behaviour(application).

%% Application callbacks
-export([start/0, start/2, stop/1]).

%% ===================================================================
%% Application callbacks
%% ===================================================================
start() ->
  ok = ensure_started(sync),
  application:start('7drl').

start(_StartType, _StartArgs) ->
  Port = get_env(port, 8008),
  runemaster:start_link([], [{port, Port}]),

  io:format("~p is running on port ~p~n", [runemaster, Port]),

  '7drl_sup':start_link().

stop(_State) ->
  ok.

get_env(Param, DefaultValue) ->
  case application:get_env('7drl', Param) of
    {ok, Val} -> Val;
    undefined -> DefaultValue
  end.

ensure_started(App) ->
  case application:start(App) of
    ok ->
      ok;
    {error, {already_started, App}} ->
      ok
  end.