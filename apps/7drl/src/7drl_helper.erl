-module('7drl_helper').
-compile(export_all).

shuffle(List) -> shuffle(List, []).
shuffle([], Acc) -> Acc;
shuffle(List, Acc) ->
   {Leading, [H | T]} = lists:split(random:uniform(length(List)) - 1, List),
   shuffle(Leading ++ T, [H | Acc]).

rand_element(List) ->
  lists:nth(random:uniform(length(List)), List).

call_tile(Map, Id, Req) ->
  entity:call(Map, {tile_component, Id}, Req).

direction(W,H,PlayerX,PlayerY,X,Y) ->
  Angle = math:atan2((PlayerY+H/2) - Y, (PlayerX+W/2) - X),
  XAngle = abs(Angle) - math:pi()/2,

  DirX = try trunc((XAngle/abs(XAngle))*(max(abs(XAngle)-math:pi()/8,0)/(abs(XAngle)-math:pi()/8))) of
    Eval -> Eval
  catch
    error:badarith -> 0
  end,

  DirY = try -trunc((Angle/abs(Angle))*(max(modulo(abs(Angle),(math:pi()*7/8))-math:pi()/8,0)/(modulo(abs(Angle), (math:pi()*7/8))-math:pi()/8))) of
    Evaly -> Evaly
  catch
    error:badarith ->  0
  end,

  {DirX,DirY}.

modulo(A,B) ->
  A-(trunc(A/B)*B).