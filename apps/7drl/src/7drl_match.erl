-module('7drl_match').
-behaviour(gen_component).

-export([ init/1,
          handle_event/2,
          handle_call/2]).

-export([start_link/5, move/6, spawn_location/1, place_rune/2]).
-record (state, {name, players=[],runes=[], instances=[]}).

start_link(Name, TileWidth, TileHeight, MapWidth, MapHeight) ->
  {ok, Pid} = entity:start_link(),
  entity:add_component(Pid, ?MODULE, [Name]),

  XTiles = lists:seq(0, MapWidth, TileWidth),
  YTiles = lists:seq(0, MapHeight, TileHeight),

  Runes = lists:map(fun (_) -> create_rune(Pid) end, lists:seq(1, 7)),
  random:seed(erlang:now()),
  % Generate map data.
  MapData = generate_map("Medium"),
  % Populate the tiles with map data.
  Tiles = [add_tile(Pid, X, Y, TileWidth, TileHeight, MapData, Runes) ||
        X <- XTiles, Y <- YTiles, X < MapWidth, Y < MapHeight],
  % Generate random runes.
  RuneEffects = [{fire_component, {Pid,0,0}},
               {pit_component, {Pid,0,0}},
               {portal_component, {Pid,0,0}},
               {remove_wall_component, {Pid,0,0}},
               {teleother_component, {Pid,0,0}},
               {teleport_component, {Pid,0,0}},
               {wall_component, {Pid,0,0}}],

  % Populate the rune entities with random effect each.
  RandomEffects = '7drl_helper':shuffle(RuneEffects),
  lists:foldl(fun
    (Rune, [{Mod, Args}|Rest]) ->
      entity:add_component(Rune, Mod, Args),
      Rest
  end, RandomEffects, Runes),

  % Places 20 runes on random tiles.
  WalkableTiles = '7drl_helper':shuffle(available_tiles(Pid)),
  lists:foldl(fun
    (_, [{_, {X, Y}}|Rest]) ->
      RandRune = '7drl_helper':rand_element(Runes),
      entity:trigger(Pid, {<<"Move">>, {rune, RandRune}, {X,Y}, {X,Y}}),
      entity:trigger(RandRune, {<<"Move">>, Pid, X, Y}),
      Rest
  end, WalkableTiles, lists:seq(1,50)),
  entity:trigger(Pid, {<<"MaintainRunes">>, Runes}),
  {ok, Pid}.

move(Map, Player, X, Y, DirX, DirY) ->
  {W, H} = entity:call(Map, {tile_component, {X, Y}}, <<"Dimension">>),
  Dest = {X+DirX*W, Y+DirY*H},
  entity:trigger(Map, {<<"Move">>, {player, Player}, {X, Y}, Dest}).

spawn_location(Map) ->
  WalkableTiles = available_tiles(Map),
  {_, Location} = '7drl_helper':rand_element(WalkableTiles),
  Location.

init([Name]) ->
  {ok, #state{name=Name}}.

handle_event({<<"Move">>, {player, Pid}, _, _},
              State=#state{players=Players}) ->
  NewPlayers = case lists:member(Pid, Players) of
    true -> Players;
    false -> Players ++ [Pid]
  end,
  {ok, State#state{players=NewPlayers}};

handle_event({<<"MaintainRunes">>, Runes}, State) ->
  {ok, State#state{runes=Runes}};

% When a rune disappears, maintain new runes if needed.
handle_event({<<"Disappear">>, {rune, Pid}, _},
            State=#state{players=Players,runes=Runes}) ->
  RandRune = '7drl_helper':rand_element(Runes),
  erlang:spawn(?MODULE, place_rune, [RandRune, self()]),
  {ok, State}.

handle_call(<<"Players">>, State=#state{players=Players}) ->
  {ok, Players, State}.

place_rune(Rune, Map) ->
  Tiles = available_tiles(Map),
  {_, RandLoc={X,Y}} = '7drl_helper':rand_element(Tiles),
  entity:trigger(Map, {<<"Move">>, {rune, Rune}, RandLoc, RandLoc}),
  entity:trigger(Rune, {<<"Move">>, Map, X, Y}).

available_tiles(Map) ->
  lists:filter(fun
    ({tile_component, {X, Y}}) ->
      '7drl_helper':call_tile(Map, {X, Y}, {<<"IsPlaceable">>,wall});
    (_) -> false
  end, entity:components(Map)).

add_tile(Map, X, Y, W, H, Data, Runes) when (X div W) rem 3 == 0 ->
  % Entities = case get_entity((X div W)+1, (Y div H)+1, Data) of
  %   [] ->
  %     Rune = '7drl_helper':rand_element(Runes),
  %     Handlers = entity:which_handlers(Rune),
  %     case lists:member(portal_component, Handlers) of
  %       true -> ok;
  %       false ->
  %         entity:add_handler(Rune, portal_component, {Map, X, Y})
  %     end,

  %     entity:trigger(Rune, {<<"Move">>, Map, X, Y}),
  %     [{rune, Rune}];
  %   Wall -> Wall
  % end,
  % Args = {X, Y, W, H, Entities},
  Args = {X, Y, W, H, get_entity((X div W)+1, (Y div H)+1, Data)},
  entity:add_component(Map, {tile_component, {X, Y}}, Args);

add_tile(Map, X, Y, W, H, Data, _Runes) ->
  Args = {X, Y, W, H, get_entity((X div W)+1, (Y div H)+1, Data)},
  entity:add_component(Map, {tile_component, {X, Y}}, Args).

get_entity(X, Y, Data) ->
  case lists:nth(X, lists:nth(Y, Data)) of
    <<>> ->
      {ok, Pid} = entity:start_link(),
      [{wall, Pid}];
    <<"F">> -> []
  end.

create_rune(Map) ->
  {ok, Pid} = entity:start_link(),
  entity:add_component(Pid, rune_component, Map),
  Pid.

post(Url, Body) ->
  inets:start(),
  Request = {Url, [], "application/x-www-form-urlencoded", Body},

  case httpc:request(post, Request, [], [{body_format, binary}]) of
    {ok, {{_Version, 200, _Reason}, _Headers, Response}} ->
      {ok, Response};
    {error, Reason} -> {error, Reason}
  end.

generate_map(Size) ->
  % inets:start(),
  % MapPageUrl = "http://donjon.bin.sh/d20/dungeon/index.cgi",
  % Seed = "seed=1363479056",
  % MapStyle = "&map_style=Standard",
  % Grid = "&grid=Square",
  % DungeonLayout = "&dungeon_layout=Cavernous",

  % Params = Seed++"&dungeon_size="++Size++MapStyle++Grid++DungeonLayout++"&construct=Construct",
  % {ok, MapData} = case post(MapPageUrl, Params) of
  %   {error, _Error} -> load_local_map();
  %   {ok, MapPage} ->
  %     RegEx = "action=\"/fantasy/dungeon/lib/text.cgi\"><input type=\"hidden\" name=\"load\" value=\"([\\w]+)\" />",
  %     {match, [_, MapId]} = re:run(MapPage, RegEx, [{capture, all, list}]),

  %     MapDownloadUrl = "http://donjon.bin.sh/fantasy/dungeon/lib/text.cgi",
  %     case post(MapDownloadUrl, "load="++MapId) of
  %       {error, _E} -> load_local_map();
  %       {ok, Ok} -> {ok, Ok}
  %     end
  % end,
  {ok, MapData} = load_local_map(),
  Rows = re:split(MapData, "\n"),
  [re:split(Row, "\t") || Row <- Rows].

load_local_map() ->
  MapDir = code:priv_dir('7drl')++"/maps",
  {ok, Files} = file:list_dir(MapDir),
  MapFiles = lists:filter(fun(File) -> re:run(File, ".txt$") =/= nomatch end, Files),
  MapFile = '7drl_helper':rand_element(MapFiles),
  file:read_file(MapDir++"/"++MapFile).


