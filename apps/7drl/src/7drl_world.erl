-module('7drl_world').
-behaviour(supervisor).

-export([start_link/0,get_match/0]).

%% Supervisor callbacks
-export([init/1]).

%% Helper macro for declaring children of supervisor
-define(CHILD(I, Type), {I, {I, start_link, []}, permanent, 5000, Type, [I]}).

%% ===================================================================
%% API functions
%% ===================================================================

start_link() ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, []).

start_match(Name, TileWidth, TileHeight, MapWidth, MapHeight) when is_binary(Name) ->
  Restart = transient,
  Shutdown = 2000,
  ChildType = worker,
  StartFunc = {'7drl_match', start_link,
              [Name, TileWidth, TileHeight, MapWidth, MapHeight]},

  Modules = dynamic,

  Spec = {Name, StartFunc, Restart, Shutdown, ChildType, Modules},
  supervisor:start_child(?MODULE, Spec).

get_match() ->
  case supervisor:which_children(?MODULE) of
    [] ->
      % No match was created before, create a new one.
      Name = base64:encode(crypto:strong_rand_bytes(10)),
      start_match(Name, 16, 16, 16*57, 16*57);
    Matches ->
      Availables = lists:filter(fun
        ({_,Pid,_,_}) ->
          Players = entity:call(Pid, '7drl_match', <<"Players">>),
          length(Players) < 7
      end, Matches),

      case Availables of
        [] ->
          Name = base64:encode(crypto:strong_rand_bytes(10)),
          start_match(Name, 16, 16, 16*57, 16*57);
        Some ->
          {_,Pid,_,_} = '7drl_helper':rand_element(Some),
          {ok, Pid}
      end
  end.


%% ===================================================================
%% Supervisor callbacks
%% ===================================================================

init([]) ->
  RestartStrategy = one_for_one,
  MaxRestarts = 10,
  MaxSecondsBetweenRestarts = 3600,

  % All matches are added after upstart when world is initialized
  SupFlags = {RestartStrategy, MaxRestarts, MaxSecondsBetweenRestarts},
    {ok, {SupFlags, []}}.

