-module(effect_component).

-behaviour (gen_component).

-export([ init/1,
          handle_event/2,
          handle_call/2]).

-include ("roguelike.hrl").

% -record (state, {effect, duration=1, x, y, r=0, onHit}).

init(Record=#effect{duration=1}) ->
  {ok, Record};

init(Record=#effect{duration=infinite}) ->
  {ok, Record};

init(Record=#effect{duration=Duration}) ->
  timer:apply_after(Duration, gen_event, stop, [self()]),
  {ok, Record};

init(Record) ->
  {ok, Record}.

handle_event({<<"EntityAppeared">>, Entity, _From, {EntityX, EntityY}},
    State=#effect{x=X,y=Y,r=R,action=Action})
    when  EntityX >= X - R, EntityX =< X + R,
          EntityY >= Y - R, EntityY =< Y + R, Action =/= undefined ->

  Action(Entity),
  {ok, State}.

handle_call(<<"Effect">>, State=#effect{name=Effect, duration=1}) ->
  timer:apply_after(10, gen_event, stop, [self()]),
  {ok, Effect, State};

handle_call(<<"Effect">>, State=#effect{name=Effect, duration=Duration}) ->
  {ok, Effect, State}.