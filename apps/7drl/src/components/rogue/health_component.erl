-module (health_component).
-behaviour(gen_component).

-export([init/1, handle_event/2, handle_call/2]).

init(HP) ->
  {ok, HP}.

handle_event({<<"Hit">>, Damage}, HP) when Damage < HP ->
  NewHP = HP - Damage,
  {ok, NewHP};

handle_event({<<"Hit">>, _Damage}, 0) ->
  {ok, 0};

handle_event({<<"Hit">>, Damage}, HP) ->
  timer:apply_after(10, gen_event, stop, [self()]),
  {ok, 0}.

handle_call(<<"HP">>, HP) -> {ok, HP, HP}.