-module (identify_component).
-behaviour (gen_component).

-export([ init/1,
          handle_event/2,
          handle_call/2,
          handle_info/2]).

-record (state, {x, y, r, fov=dict:new()}).

init({Possibles, Known})->
  random:seed(erlang:now()),
  {ok, {'7drl_helper':shuffle(Possibles), Known}};

init(Args) ->
  {error, <<"invalid_args">>}.

% Whenever knowing an entity is encountered, if it's a rune,
% try to identify its symbol
handle_event({<<"EntityInView">>, {rune, Pid}},
            {Possibles=[H|Rest], Known}) ->
  case dict:find(Pid, Known) of
    {ok, Symbol} -> {ok, {Possibles, Known}};
    error -> {ok, {Rest, dict:store(Pid, H, Known)}}
  end.

handle_call({<<"Symbol">>, {rune, Pid}}, State={_, Known}) ->
  case dict:find(Pid, Known) of
    {ok, Symbol} -> {ok, Symbol, State};
    error -> {ok, <<"?">>, State}
  end;

handle_call({<<"Symbol">>, {player, Pid}}, State) ->
  {ok, <<"@">>, State}.

% If a rune is removed, try to forget its symbol.
handle_info({'EXIT',Entity,normal}, {Possibles, Known}) ->
  {ok, {Possibles, dict:erase(Entity, Known)}}.
