-module (runemaster_component).
-behavior(gen_component).

-export([ init/1,
          handle_event/2,
          handle_call/2]).

-include ("roguelike.hrl").

-record (state, {rune = undefined, xp = dict:new()}).

init(_) ->
  {ok, #state{}}.

% TODO: Sometimes it picked up the rune, but the Disappear
% event fails to make it disappear.
handle_event({<<"ItemEncountered">>, Map, Rune, At},
            State=#state{rune=undefined}) ->
  Event = {<<"Disappear">>, {rune, Rune}, At},
  entity:trigger(Map, Event),
  {ok, State#state{rune=Rune}};

% When the current player entity is told to executed a secondary
% action and it has a rune, activate it.
% TODO: Because the map triggers this event, it is received by
% multiple tiles. Fixed it.
handle_event({<<"SecondaryAction">>, Map, From, Target},
            State=#state{rune=Rune,xp=XP}) when is_pid(Rune) ->
  XPResult = dict:find(Rune,XP),
  case XPResult of
    {ok,Value} -> RuneXP = Value;
    error -> RuneXP = 0
  end,
  entity:trigger(Rune,
          #activation{by=self(), map=Map, xp=RuneXP, at=From, to=Target}),
  {ok, State};

handle_event({<<"RuneCompleted">>, Rune},
            State=#state{rune=Rune,xp=XP}) ->
  NewXP = dict:update(Rune, fun
    (Old) when Old < 5 ->
      Old + 1;
    (Old) ->
      Old
  end, 1, XP),
  {ok, State#state{rune = undefined, xp = NewXP}};

handle_event(_Event, State) ->
  {ok, State}.


handle_call(<<"Rune">>, State=#state{rune=Rune}) ->
  {ok, Rune, State}.
