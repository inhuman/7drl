-module(shotgun_component).
-behavior(gen_component).

-export([ init/1,
          handle_event/2,
          handle_call/2]).

-include ("roguelike.hrl").
-record (state, {dmg}).

init(DMG) ->
  {ok, #state{dmg = DMG}}.

% When the current player entity is told to executed a secondary
% action and it has a rune, activate it.
handle_event({<<"PrimaryAction">>, Map, {PlayerX, PlayerY}, {X, Y}},
            State=#state{dmg=DMG}) ->
  {W,H} = entity:call(Map, {tile_component, {PlayerX,PlayerY}}, <<"Dimension">>),

  {DirX, DirY} = '7drl_helper':direction(W,H,PlayerX,PlayerY,X,Y),

  PlayerTiles = [{X,Y} ||
  X <- lists:seq(PlayerX-W,PlayerX+W,W),
  Y <- lists:seq(PlayerY-H,PlayerY+H,H)],
  ShotTiles = lists:map(fun({X,Y}) -> {X+DirX*W*2,Y+DirY*H*2} end,PlayerTiles),
  ShuffledTiles = '7drl_helper':shuffle(ShotTiles),
  AffectedTiles = lists:sublist(ShuffledTiles,5),

  lists:foreach(
    fun({TileX, TileY}) ->
      Entities = entity:call(Map, {tile_component, {TileX, TileY}},<<"TileEntities">>),
      [entity:trigger(Entity, {<<"Hit">>, DMG}) || {_Type,Entity} <- Entities],
      entity:trigger(Map, {<<"Move">>, create_effect_entity(TileX,TileY), {TileX,TileY}, {TileX, TileY}})
    end
  , AffectedTiles),

  {ok, State}.

handle_call(<<"Damage">>, State=#state{dmg=Damage}) ->
  {ok, Damage, State}.

create_effect_entity(X,Y) ->
  {ok, Entity} = entity:start_link(),
  entity:add_component(Entity, effect_component, #effect{name= <<"shot">> ,x=X,y=Y,duration=150}),
  {effect, Entity}.
