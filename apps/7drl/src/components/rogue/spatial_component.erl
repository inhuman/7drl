-module(spatial_component).

-behavior(gen_component).

-export([ init/1,
          handle_event/2,
          handle_call/2,
          handle_info/2]).

-record (state, {x, y, r, fov=dict:new(), defaultr, timer}).

init({X, Y, Radius}) when is_number(X),is_number(Y)->
  {ok, #state{x=X, y=Y, r=Radius, defaultr=Radius}};

init(Args) ->
  {error, <<"invalid_args">>}.

% When this player entity actually moves to some where, store it.
handle_event({<<"Move">>, Map, DestX, DestY},
              State=#state{r=R, fov=Fov}) ->
  {Tw, Th} = '7drl_helper':call_tile(Map, {DestX,DestY}, <<"Dimension">>),

  TopLeftX = DestX - R,
  TopLeftY = DestY - R,

  Surroundings =
    [{X, Y, '7drl_helper':call_tile(Map, {X,Y}, <<"TileEntities">>)} ||
      X <- lists:seq(DestX-R, DestX+R, Tw),
      Y <- lists:seq(DestY-R, DestY+R, Th)],

  NewFov = lists:foldl(fun
    ({_,_,{error, bad_module}}, FovSoFar) ->
      % When the FOV is out of map bound.
      FovSoFar;
    ({X,Y,Entities}, FovSoFar) ->
      [entity:trigger(self(), {<<"EntityInView">>, E}) || E <- Entities],
      dict:store({X-TopLeftX, Y-TopLeftY}, Entities, FovSoFar)
  end, Fov, Surroundings),

  {ok, State#state{x=DestX, y=DestY, fov=NewFov}};

% When some entity that appears within my FOV, keeps track of it,
handle_event({<<"EntityAppeared">>, Entity, {FromX, FromY}, {DestX, DestY}},
            State=#state{x=X,y=Y,r=Radius, fov=Fov})
            when  DestX >= X - Radius, DestX =< X + Radius,
                  DestY >= Y - Radius, DestY =< Y + Radius ->
  entity:trigger(self(), {<<"EntityInView">>, Entity}),
  {_, Pid} = Entity,
  erlang:link(Pid),

  FromKey = {FromX - (X - Radius), FromY - (Y - Radius)},
  DestKey = {DestX - (X - Radius), DestY - (Y - Radius)},

  % Remove the entity from FOV if any.
  NewFov = case dict:find(FromKey, Fov) of
    {ok, Entities} ->
      dict:store(FromKey, Entities -- [Entity], Fov);
    error -> Fov
  end,

  {ok, State#state{fov=dict:append(DestKey, Entity, NewFov)}};

% For effect that not in FOV, we still keep track of it.
handle_event({<<"EntityAppeared">>, {effect, Pid}, {FromX, FromY}, {DestX, DestY}},
              State=#state{x=X,y=Y,r=Radius,fov=Fov}) ->
  erlang:link(Pid),
  FromKey = {FromX - (X - Radius), FromY - (Y - Radius)},
  DestKey = {DestX - (X - Radius), DestY - (Y - Radius)},
  {ok, State#state{fov=dict:append(DestKey, {effect, Pid}, Fov)}};

% If not in fov, removes the entity whether it was in FOV or not.
handle_event({<<"EntityAppeared">>, Entity, {FromX, FromY}, {DestX, DestY}},
              State=#state{x=X,y=Y,r=Radius,fov=Fov}) ->
  Key = {FromX - (X - Radius), FromY - (Y - Radius)},
  NewFov = case dict:find(Key, Fov) of
    {ok, Entities} -> dict:store(Key, Entities -- [Entity], Fov);
    error -> Fov
  end,
  {ok, State#state{fov=NewFov}};

handle_event({<<"Disappear">>, Entity, {SomeX,SomeY}},
              State=#state{x=X,y=Y,r=Radius,fov=Fov}) ->
  Key = {SomeX - (X - Radius), SomeY - (Y - Radius)},
  NewFov = case dict:find(Key, Fov) of
    {ok, Entities} -> dict:store(Key, Entities -- [Entity], Fov);
    error -> Fov
  end,
  {ok, State#state{fov=NewFov}};

handle_event({<<"ChangeRadius">>, Radius, -1, Map}, State=#state{x=X,y=Y}) ->
  timer:apply_after(0, entity, trigger, [self(),{<<"Move">>, Map, X, Y}]),
  {ok, State#state{r=Radius, timer={}}};

handle_event({<<"ChangeRadius">>, Radius, Duration, Map}, State=#state{defaultr=DefaultR,timer=Timer,x=X,y=Y}) ->
  case is_reference(Timer) of
    false ->
      NewTimer = erlang:send_after(Duration, self(), {<<"ResetRadius">>,DefaultR, Map});
    true ->
      erlang:cancel_timer(Timer),
      NewTimer = erlang:send_after(Duration, self(), {<<"ResetRadius">>,DefaultR, Map})
  end,
  timer:apply_after(0, entity, trigger, [self(),{<<"Move">>, Map, X, Y}]),
  {ok, State#state{r=Radius, timer=NewTimer}}.

handle_call(<<"Position">>, State=#state{x=X, y=Y}) ->
  {ok, {X, Y}, State};

handle_call(<<"FOV">>, State=#state{x=X,y=Y,r=Radius,fov=Fov}) ->
  TopLeftX = X - Radius,
  TopLeftY = Y - Radius,

  Result = lists:flatmap(fun
    ({{RelX, RelY}, Entities}) ->
      [{{RelX+TopLeftX, RelY+TopLeftY},Entity} || Entity <- Entities]
  end, dict:to_list(Fov)),
  {ok, {X, Y, Radius, Result}, State}.

handle_info({<<"ResetRadius">>, Radius, Map}, State) ->
  % Don't do anything, just let other handle_event to update the radius
  % and trigger Move.
  entity:trigger(self(), {<<"ChangeRadius">>, Radius, -1, Map}),
  {ok, State};

handle_info({'EXIT',Entity,normal}, State=#state{fov=Fov}) ->
  NewFov = dict:map(fun({RelX, RelY}, Entities) ->
    Entities -- [
                  {player,Entity},
                  {rune,Entity},
                  {effect,Entity},
                  [wall, Entity]]
  end, Fov),
  {ok, State#state{fov=NewFov}}.