-module (fire_component).
-behavior(gen_component).

-export([ init/1,
          handle_event/2,
          handle_info/2]).

-include ("roguelike.hrl").
-record (state, {map, x, y}).

init({Map, X, Y}) when is_pid(Map), is_number(X), is_number(Y)->
  {ok, #state{map=Map, x=X, y=Y}};

init(_Args) ->
  {error, <<"invalid_args">>}.

handle_event(#activation{by=By, map=Map, at=At, to={TargetX,TargetY}},
            State=#state{map=Map}) ->
  {W,H} = entity:call(Map, {tile_component, {TargetX,TargetY}}, <<"Dimension">>),

  FireTiles = [{X,Y} ||
  X <- lists:seq(TargetX-W*2,TargetX+W*2,W),
  Y <- lists:seq(TargetY-H*2,TargetY+H,H)],

  {ok, Fire} = entity:start_link(),

  FireAction = fun
        ({player, Pid}) ->
          create_smoke(FireTiles, Map);
        (_) ->
          ok
  end,

  FireEffect = #effect{name= <<"^fire">>, duration=25000, x=TargetX, y=TargetY, action=FireAction},
  entity:add_component(Fire, effect_component, FireEffect),
  entity:trigger(Map, {<<"Move">>, {effect,Fire}, {TargetX,TargetY}, {TargetX,TargetY}}),
  erlang:send_after(1000, self(), {<<"Burn">>,FireTiles,Fire,{TargetX,TargetY}}),
  timer:apply_after(0, entity, trigger, [By, {<<"RuneCompleted">>, self()}]),

  {ok, State}.

handle_info({<<"Burn">>, Tiles, Fire, Target}, State=#state{map=Map}) ->
  case {is_pid(Fire),is_process_alive(Fire)} of
    {true,true} ->
      create_smoke(Tiles,Map),
      Entities = entity:call(Map, {tile_component, Target},<<"TileEntities">>),
      [entity:trigger(Entity, {<<"Hit">>, 5}) || {_Type,Entity} <- Entities],
      erlang:send_after(1000, self(), {<<"Burn">>,Tiles,Fire,Target}),
      {ok, State};
    _ ->
      {ok, State}
  end.

%% ==================================================================
%% Internal Funtions
%% ==================================================================

create_smoke(Tiles, Map) ->
  ShuffledTiles = '7drl_helper':shuffle(Tiles),
  SmokeTiles = lists:sublist(ShuffledTiles,3),

  lists:foreach(
    fun({TileX, TileY}) ->
      SmokeAction = fun
        ({player, Pid}) ->
          entity:trigger(Pid, {<<"ChangeRadius">>, 32, 5000, Map});
        (_) ->
          ok
      end,
      SmokeEffect = #effect{name= <<"%smoke">>, duration=5000, x=TileX, y=TileY, action=SmokeAction},
      {ok, Smoke} = entity:start_link(),
      entity:add_component(Smoke, effect_component, SmokeEffect),
      entity:trigger(Map, {<<"Move">>, {effect,Smoke}, {TileX,TileY}, {TileX,TileY}})
    end
  ,SmokeTiles).
