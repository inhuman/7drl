-module (pit_component).
-behavior(gen_component).

-export([ init/1,
          handle_event/2]).

-include ("roguelike.hrl").
-record (state, {map, x, y}).

init({Map, X, Y}) when is_pid(Map), is_number(X), is_number(Y)->
  {ok, #state{map=Map, x=X, y=Y}};

init(_Args) ->
  {error, <<"invalid_args">>}.

handle_event(#activation{by=By, map=Map, at={PlayerX,PlayerY}, to={TargetX,TargetY}},
            State=#state{map=Map}) ->
  {W,H} = entity:call(Map, {tile_component, {PlayerX,PlayerY}}, <<"Dimension">>),

  case TargetX >= PlayerX-W andalso TargetX =< PlayerX+W andalso TargetY >= PlayerY-H andalso TargetY =< PlayerY+H of
    true ->
      Entities = '7drl_helper':call_tile(Map, {TargetX,TargetY}, <<"TileEntities">>),
      [destroy_contents(Map, Entity, {TargetX,TargetY}) || Entity <- Entities ],
      {ok, Pit} = entity:start_link(),

      Action = fun
        ({player, Pid}) ->
          entity:trigger(Pid, {<<"Hit">>, 9999});
          % Do something with player;
        ({rune, Pid}) ->
          entity:trigger(Map, {<<"Disappear">>, {rune, Pid}, {TargetX,TargetY}});
          % do;
        (Test) ->
          ok
      end,

      Effect = #effect{name= <<"0pit">>, duration=infinite, x=TargetX, y=TargetY, action=Action},
      entity:add_component(Pit, effect_component, Effect),
      entity:trigger(Map, {<<"Move">>, {effect,Pit}, {TargetX,TargetY}, {TargetX,TargetY}}),
      entity:trigger(By, {<<"RuneCompleted">>, self()}),
      {ok, State};
    false ->
      {ok, State};
    Test ->
      {ok,State}
  end.

%% ==================================================================
%% Internal Funtions
%% ==================================================================

destroy_contents(Map, Entity, Target) ->
  case Entity of
    {player, Remove} ->
      % gen_event:stop(Remove);
      entity:trigger(Remove, {<<"Hit">>,9999});
    {rune, Remove} ->
      entity:trigger(Map, {<<"Disappear">>, {rune, Remove}, Target});
    _ ->
      nothing
  end.