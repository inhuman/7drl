-module (portal_component).
-behavior(gen_component).

-export([ init/1,
          handle_event/2]).

-include ("roguelike.hrl").

-define(DIRECTIONS, [{0,-1}, {1,-1}, {1,0}, {1,1}, {0,1}, {-1,1}, {-1,0}, {-1,-1}]).
-record (state, {map, x, y, pos={}}).

init({Map, X, Y}) when is_pid(Map), is_number(X), is_number(Y)->
  {ok, #state{map=Map, x=X, y=Y}};

init(_Args) ->
  {error, <<"invalid_args">>}.

handle_event(#activation{by=By, map=Map, at=At, to=Target},
            State=#state{map=Map, pos={}}) ->
  {ok, State#state{pos=Target}};

handle_event(#activation{by=By, map=Map, at=At, to={TargetX,TargetY}},
            State=#state{map=Map, pos={FirstX,FirstY}}) ->
  {W,H} = entity:call(Map, {tile_component, {FirstX,FirstY}}, <<"Dimension">>),
  FirstPlaceable = entity:call(Map, {tile_component, {FirstX,FirstY}}, {<<"IsPlaceable">>,effect}),
  SecondPlaceable = entity:call(Map, {tile_component, {TargetX,TargetY}}, {<<"IsPlaceable">>,effect}),
  Range = not(TargetX >= FirstX - W andalso TargetX =< FirstX + W andalso TargetY >= FirstY - H andalso TargetY =< FirstY + H),

  case {FirstPlaceable,SecondPlaceable,Range} of
    {true,true,true} ->
      Portal1Entities = '7drl_helper':call_tile(Map, {FirstX,FirstY}, <<"TileEntities">>),
      Portal2Entities = '7drl_helper':call_tile(Map, {TargetX,TargetY}, <<"TileEntities">>),
      Portal1 = create_portal(FirstX,FirstY,W,H,Map,TargetX,TargetY),
      Portal2 = create_portal(TargetX,TargetY,W,H,Map,FirstX,FirstY),
      entity:trigger(Map, {<<"Move">>, {effect,Portal1}, {FirstX,FirstY}, {FirstX,FirstY}}),
      entity:trigger(Map, {<<"Move">>, {effect,Portal2}, {TargetX,TargetY}, {TargetX,TargetY}}),
      [entity:trigger(Map, {<<"Move">>, Entity, {FirstX,FirstY}, {FirstX,FirstY}}) || Entity <- Portal1Entities],
      [entity:trigger(Map, {<<"Move">>, Entity, {TargetX,TargetY}, {TargetX,TargetY}}) || Entity <- Portal2Entities],
       timer:apply_after(0, entity, trigger, [By, {<<"RuneCompleted">>, self()}]);
    _ ->
      ok
  end,
  {ok, State#state{pos={}}}.

%% ==================================================================
%% Internal Funtions
%% ==================================================================
create_portal(EntX,EntY,W,H,Map,ExitX,ExitY) ->
  PortalAction = fun
    ({Type,Pid}) ->
      Neighbors = [{ExitX+DirX*W, ExitY+DirY*H} || {DirX, DirY} <- ?DIRECTIONS],
      PortalTiles = [{X,Y} || {X,Y} <- Neighbors,
      entity:call(Map, {tile_component, {X,Y}}, {<<"IsPlaceable">>,effect})],
      ShuffledTiles = '7drl_helper':shuffle(PortalTiles),
      [SelectedTile] = lists:sublist(ShuffledTiles,1),
      entity:trigger(Map, {<<"Move">>, {Type,Pid}, {EntX,EntY}, SelectedTile});
    (_) ->
      ok
    end,

  PortalEffect = #effect{name= <<"0portal">>, duration=100000, x=EntX, y=EntY, action=PortalAction},
  {ok, Portal} = entity:start_link(),
  entity:add_component(Portal, effect_component, PortalEffect),
  Portal.


