-module (remove_wall_component).
-behavior(gen_component).

-export([ init/1,
          handle_event/2]).

-include ("roguelike.hrl").
-record (state, {map, x, y}).

init({Map, X, Y}) when is_pid(Map), is_number(X), is_number(Y)->
  {ok, #state{map=Map, x=X, y=Y}};

init(_Args) ->
  {error, <<"invalid_args">>}.

handle_event(#activation{by=By, map=Map, at=At, to=Target},
            State=#state{map=Map}) ->

  Entities = entity:call(Map, {tile_component, Target},<<"TileEntities">>),

  case Entities of
    [{wall, Entity}|_] ->
      entity:trigger(Map, {<<"Disappear">>, {wall,Entity}, Target}),
      timer:apply_after(0, entity, trigger, [By, {<<"RuneCompleted">>, self()}]),
      {ok, State};
    _ -> {ok, State}
  end.