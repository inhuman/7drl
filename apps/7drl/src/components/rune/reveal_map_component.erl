-module (reveal_map_component).
-behavior(gen_component).

-export([ init/1,
          handle_event/2,
          handle_info/2]).

-include ("roguelike.hrl").
-record (state, {map, x, y, duration=15000}).

init({Map, X, Y}) when is_pid(Map), is_number(X), is_number(Y)->
  {ok, #state{map=Map, x=X, y=Y}};

init(_Args) ->
  {error, <<"invalid_args">>}.

handle_event(#activation{by=By, map=Map, at=At, to=Target},
  State=#state{duration=Duration}) ->
  entity:trigger(By, {<<"ChangeRadius">>, 240}),
  entity:trigger(Map, {<<"Move">>, {player, By}, At, At}),
  erlang:send_after(Duration, self(), {endduration,By}),
  {ok, State}.

handle_info({endduration, By}, State) ->
  entity:trigger(By, {<<"ChangeRadius">>, 64}),
  timer:apply_after(0, entity, trigger, [By, {<<"RuneCompleted">>, self()}]),
  {ok, State}.
