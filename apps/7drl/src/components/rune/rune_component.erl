-module (rune_component).
-behavior(gen_component).

-export([ init/1,
          handle_event/2]).

-record (rune, {map, owners = dict:new()}).

% This is the map where it is created.
% Other components which handle the logic keeps track of another
% map pid, where it is activated. It maybe the same map.
init(Map) when is_pid(Map)->
  {ok, #rune{map=Map}};

init(_Args) ->
  {error, <<"invalid_args">>}.

% When a new rune of this type is placed on the map, marks its
% location with an undefined owner.
handle_event({<<"Move">>, _Map, NewX, NewY},
              Rune=#rune{owners=Owners}) ->
  case dict:find({NewX, NewY}, Owners) of
    error ->
      {ok, Rune#rune{
            owners=dict:store({NewX,NewY}, undefined, Owners)}};
    Sth ->
      {ok, Rune}
  end;

% When a player steps on one of runes of this type on the map,
% marks the player as the owner of the rune at that location.
handle_event({<<"EntityAppeared">>, {player, Pid}, _From, {X, Y}},
            Rune=#rune{map=Map, owners=Owners}) ->
  case dict:find({X, Y}, Owners) of
    error -> {ok, Rune};
    {ok, undefined} ->
      entity:trigger(Pid,
        {<<"ItemEncountered">>, Map, self(), {X, Y}}),
      {ok, Rune#rune{owners=dict:store({X,Y}, Pid, Owners)}};
    {ok, _} -> {ok, Rune}
  end;

% When the player picks up this rune instance, the map will
% send this event with the location of pick up.
% This event is also fired when the rune is moved by teleport.
handle_event({<<"Disappear">>, Entity, At},
              Rune=#rune{owners=Owners}) ->
  case dict:find(At, Owners) of
    error -> {ok, Rune};
    {ok, _} ->
      {ok, Rune#rune{owners=dict:erase(At, Owners)}}
  end.