-module (teleother_component).
-behavior(gen_component).

-export([ init/1,
          handle_event/2]).

-include ("roguelike.hrl").
-record (state, {map, x, y, pos={}}).

init({Map, X, Y}) when is_pid(Map), is_number(X), is_number(Y)->
  {ok, #state{map=Map, x=X, y=Y}};

init(_Args) ->
  {error, <<"invalid_args">>}.

handle_event(#activation{by=By, map=Map, at=At, to=Target},
            State=#state{map=Map, pos={}}) ->
  Placeable = entity:call(Map, {tile_component, Target}, {<<"IsPlaceable">>,effect}),
  case Placeable of
    true ->
      {ok, State#state{pos=Target}};
    false ->
      {ok, State#state{pos={}}}
  end;


handle_event(#activation{by=By, map=Map, at=At, to=Target},
            State=#state{map=Map, pos=FirstClick}) ->

  Entities = '7drl_helper':call_tile(Map, FirstClick, <<"TileEntities">>),
  Condition = {entity:call(Map, {tile_component, Target},{<<"IsPlaceable">>,effect}),
              Entities},

  case Condition of
    {false,_} ->
      {ok, State#state{pos={}}};
    {true,[]} ->
      {ok, State#state{pos={}}};
    {true,_} ->
      [entity:trigger(Map, {<<"Move">>, Entity, FirstClick, Target}) || Entity <- Entities],
      timer:apply_after(0, entity, trigger, [By, {<<"RuneCompleted">>, self()}]),
      {ok, State#state{pos={}}}
  end.