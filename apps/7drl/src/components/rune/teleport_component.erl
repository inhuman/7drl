-module (teleport_component).

-behavior(gen_component).

-export([ init/1,
          handle_event/2]).

-include ("roguelike.hrl").
-record (state, {map, x, y}).

init({Map, X, Y}) when is_pid(Map), is_number(X), is_number(Y)->
  {ok, #state{map=Map, x=X, y=Y}};

init(_Args) ->
  {error, <<"invalid_args">>}.


handle_event(#activation{by=By, map=Map, xp=5, at=At, to=Target},
            State=#state{map=Map}) ->
  entity:trigger(Map, {<<"Move">>, {player, By}, At, Target}),
  timer:apply_after(0, entity, trigger, [By, {<<"RuneCompleted">>, self()}]),  
  {ok, State};

handle_event(#activation{by=By, map=Map, xp=XP, at=At, to=Target},
            State=#state{map=Map}) ->
  SelectedTile = selected_area(Target, 5-XP, Map),
  entity:trigger(Map, {<<"Move">>, {player, By}, At, SelectedTile}),
  timer:apply_after(0, entity, trigger, [By, {<<"RuneCompleted">>, self()}]),  
  {ok, State}.

selected_area({TargetX,TargetY},R,Map) ->
  {W,H} = gen_component:call(Map, {tile_component, {TargetX,TargetY}}, <<"Dimension">>),

  Area = [{X,Y} ||
  X <- lists:seq(TargetX-W*R,TargetX+W*R,W),
  Y <- lists:seq(TargetY-H*R,TargetY+H*R,H)],
  
  ValidTiles = [{TileX,TileY} || {TileX,TileY} <- Area,
  gen_component:call(Map, {tile_component, {TileX,TileY}}, {<<"IsPlaceable">>,effect})],

  ShuffledTiles = '7drl_helper':shuffle(ValidTiles),
  [SelectedTile] = lists:sublist(ShuffledTiles,1),

  SelectedTile.
