-module (wall_component).
-behavior(gen_component).

-export([ init/1,
          handle_event/2]).

-include ("roguelike.hrl").
-record (state, {map, x, y}).

init({Map, X, Y}) when is_pid(Map), is_number(X), is_number(Y)->
  {ok, #state{map=Map, x=X, y=Y}};

init(_Args) ->
  {error, <<"invalid_args">>}.

handle_event(#activation{by=By, map=Map, at=At, to=Target},
            State=#state{map=Map}) ->
  Placeable = entity:call(Map, {tile_component, Target}, {<<"IsPlaceable">>,wall}),

  case Placeable of
    true ->
      {ok, Wall} = entity:start_link(),
      entity:trigger(Map, {<<"Move">>, {wall,Wall}, Target, Target}),
      timer:apply_after(0, entity, trigger, [By, {<<"RuneCompleted">>, self()}]),
      {ok, State};
    false ->
      {ok, State}
  end.