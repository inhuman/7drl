-module(tile_component).

-behaviour(gen_component).

%% gen_event callbacks
-export([ init/1,
          handle_event/2,
          handle_call/2,
          handle_info/2]).

-include ("roguelike.hrl").

init({X, Y, W, H}) ->
  init({X, Y, W, H, []});

init({X, Y, W, H, Entities}) ->
  {Es, Walkable} = case lists:keyfind(wall, 1, Entities) of
    false -> {Entities, true};
    Wall -> {[Wall], false}
  end,
  {ok, #tile{x=X,y=Y,w=W,h=H,entities=Es,walkable=Walkable}}.

handle_event({<<"Move">>, {wall, Entity}, {X, Y}, {X, Y}},
            Tile=#tile{x=X, y=Y, entities=Entities}) ->
  % Notifies existing entities of the newly spawned entity.
  erlang:link(Entity),
  notify_others(Entities, {wall, Entity}, {X, Y}, {X, Y}),
  % Also let the entity know so that it actually moves.
  entity:trigger(Entity, {<<"Move">>, self(), X, Y}),

  {ok, Tile#tile{entities = Entities ++ [{wall, Entity}],walkable=false}};

% The tile an entity is spawned.
handle_event({<<"Move">>, {Type, Entity}, {X, Y}, {X, Y}},
            Tile=#tile{x=X, y=Y, entities=Entities}) ->
  % Notifies existing entities of the newly spawned entity.
  erlang:link(Entity),
  notify_others(Entities, {Type, Entity}, {X, Y}, {X, Y}),
  % Also let the entity know so that it actually moves.
  entity:trigger(Entity, {<<"Move">>, self(), X, Y}),

  {ok, Tile#tile{entities = Entities ++ [{Type, Entity}]}};

% When the tile is not walkable, just put the player back.
handle_event({<<"Move">>, Entity, {X, Y}, {NewX, NewY}},
            Tile=#tile{x=NewX, y=NewY, walkable=false}) ->
  entity:trigger(self(), {<<"Move">>, Entity, {X, Y}, {X, Y}}),
  {ok, Tile};

% The tile an entity is leaving.
handle_event({<<"Move">>, {Type, Entity}, {X, Y}, {NewX, NewY}},
            Tile=#tile{x=X, y=Y, entities=Entities}) ->
  % io:format("Entity ~p leaves entities ~p at tile ~p,~p~n", [Entity,Entities,X,Y]),
  erlang:link(Entity),
  entity:trigger(Entity, {<<"Disappear">>, Entity, {X, Y}}),
  % notify_others(Entities, {Type, Entity}, NewX, NewY),
  {ok, Tile#tile{entities = Entities -- [{Type, Entity}]}};

% The tile an entity is moving to.
handle_event({<<"Move">>, {Type, Entity}, {X, Y}, {NewX, NewY}},
            Tile=#tile{x=NewX,y=NewY,entities=Entities}) ->
  erlang:link(Entity),
  entity:trigger(Entity, {<<"Move">>, self(), NewX, NewY}),
  notify_others(Entities, {Type, Entity}, {X,Y}, {NewX, NewY}),
  % io:format("Entity ~p joins entities ~p at tile ~p,~p~n", [Entity,Entities,NewX,NewY]),

  NewEntities = case lists:member({player, Entity}, Entities) of
    true -> Entities;
    false -> Entities ++ [{Type, Entity}]
  end,
  {ok, Tile#tile{entities = NewEntities}};

% All other tiles on the map will notify their own entities so that they
% can update their FOV if needed.
handle_event({<<"Move">>, {EntityType, Entity}, From, To},
            Tile=#tile{entities=Entities}) ->
  erlang:link(Entity),
  notify_others(Entities, {EntityType, Entity}, From, To),
  {ok, Tile};

% When a secondary action is performed on the map, if the target is within
% this tile, tell the Entity to execute the action.
handle_event(#action{type =Type,
                     by=Entity, at=At, to={ToX, ToY}},
            Tile=#tile{x=TileX, y=TileY, w=W, h=H})
            when ToX >= TileX, ToX =< TileX + W,
                 ToY >= TileY, ToY =< TileY + H ->
  entity:trigger(Entity, {Type, self(), At, {TileX, TileY}}),
  {ok, Tile};

% When a player picks up a rune, he will let the map know.
% The map in turn tells all entities that have the rune in view so
% that they can remove it.
handle_event(Event={<<"Disappear">>, {wall, Entity}, {X, Y}},
            Tile=#tile{x=X,y=Y,entities=Entities, walkable=false}) ->

  [entity:trigger(E, Event) || {_Type, E} <- Entities],
  {ok, Tile#tile{entities = Entities -- [{wall,Entity}],walkable=true}};

handle_event(Event={<<"Disappear">>, Entity, {X, Y}},
            Tile=#tile{x=X,y=Y,entities=Entities}) ->
  [entity:trigger(E, Event) || {_Type, E} <- Entities],
  {ok, Tile#tile{entities = Entities -- [Entity]}};

handle_event(Event={<<"Disappear">>, Entity, {X, Y}},
              Tile=#tile{entities=Entities}) ->
  [entity:trigger(E, Event) || {_Type, E} <- Entities],
  {ok, Tile}.

handle_call(<<"Dimension">>, Tile=#tile{w=W, h=H}) ->
  {ok, {W, H}, Tile};

handle_call(<<"TileEntities">>, Tile=#tile{entities=Entities}) ->
  {ok, Entities, Tile};

handle_call({<<"IsPlaceable">>,wall}, Tile=#tile{entities=[],walkable=true}) ->
  {ok, true, Tile};

handle_call({<<"IsPlaceable">>,effect}, Tile=#tile{walkable=true}) ->
  {ok, true, Tile};

handle_call({<<"IsPlaceable">>,player}, Tile=#tile{walkable=true}) ->
  {ok, true, Tile};

handle_call({<<"IsPlaceable">>,_}, Tile) ->
  {ok, false, Tile}.

handle_info({'EXIT',Entity,normal}, Tile=#tile{entities=Entities}) ->
  {ok, Tile#tile{entities=Entities -- [
                                        {player, Entity},
                                        {rune, Entity},
                                        {effect, Entity},
                                        {wall, Entity}]}}.

-spec notify_others([entity()], entity(), location(), location()) -> no_return().
notify_others(Entities, Entity, From, To) ->
  Event = {<<"EntityAppeared">>, Entity, From, To},
  [entity:trigger(E, Event) || {_Type, E} <- Entities].
