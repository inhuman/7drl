-module (runemaster).
-behaviour (gen_gameserver).

%% API
-export ([start_link/2]).

%% gen_gameserver callbacks
-export ([init/1
          , player_joined/1
          , player_input/2
          , player_disconnected/1
          , frame_started/1]).

-include ("roguelike.hrl").

-record (pstate, {pid, map, input=undefined}).

%% ==================================================================
%% API
%% ==================================================================
start_link(Args, Opts) ->
  gen_gameserver:start_link(?MODULE, Args, Opts).

%% ==================================================================
%% gen_gameserver callback
%% ==================================================================
init(Args) ->
  random:seed(erlang:now()),
  {ok, Args}.

player_joined(SockPid) ->
  {ok, Map} = '7drl_world':get_match(),
  {X, Y} = '7drl_match':spawn_location(Map),

  {ok, Player} = entity:start_link([
    {spatial_component, {X, Y, 112}},
    {identify_component, {?POSSIBLE_RUNES, dict:new()}},
    {runemaster_component, []},
    {shotgun_component, 25},
    {health_component, 100}
  ]),

  entity:trigger(Map, {<<"Move">>, {player, Player}, {X, Y}, {X, Y}}),

  MapUpdate = [
            [{<<"id">>, <<"Map">>},
            {<<"data">>, [{<<"mapWidth">>,800},{<<"mapHeight">>,800},
            {<<"tileWidth">>,16},{<<"tileHeight">>,16}]}]
            ],

  {ok, MapUpdate, #pstate{pid=Player, map=Map}}.

player_input(Input, State) ->
  {ok, State#pstate{input=Input}}.

player_disconnected(State=#pstate{pid=Player}) when Player =/= undefined ->
  entity:stop(Player),
  ok;

player_disconnected(_State) ->
  ok.

frame_started(PlayerState=#pstate{pid=Player}) when is_pid(Player) ->
  case is_process_alive(Player) of
    true ->
      {ok, PlayerState1=#pstate{pid=Player}} = handle_input(PlayerState),
      {X, Y, R, Fov} = entity:call(Player, spatial_component, <<"FOV">>),

      FovData = [{position_to_key(OtherX,OtherY), get_presentation(Player, {Type, Pid})} ||
        {{OtherX, OtherY}, {Type, Pid}} <- Fov, is_pid(Pid), is_process_alive(Pid)],

      % Client will receive the following JSON: {
      %   "id": "Map",
      %   "data": {
      %     "x": X,
      %     "y": Y,
      %     "r": R,
      %     "symbols": {
      %       "1,1": "@",
      %       "1,2": "#",
      %       ...
      %     }
      %   }
      % }

      % @todo: only send changes
      FovUpdate = [
        [ {<<"id">>, <<"Map">>},
          {<<"data">>,
            [ {<<"x">>, X}, {<<"y">>,Y}, {<<"r">>,R},
              {<<"symbols">>,FovData++
                [{position_to_key(X,Y),get_presentation(Player)}]}]}]],
      {ok, FovUpdate, PlayerState1};
    false ->
      LastUpdate = [
        [ {<<"id">>, <<"Map">>},
          {<<"data">>, [{<<"symbols">>, <<"none">>}]}]],
      {ok, LastUpdate, PlayerState#pstate{pid=undefined}}
  end;

frame_started(PlayerState) ->
  skip.

% frame_ended(WorldState) ->

%% ==================================================================
%% Internal Funtions
%% ==================================================================
handle_input(PlayerState=#pstate{input=?UP_ARROW,
                                  pid=Player, map=Map}) ->
  {X, Y} = entity:call(Player, spatial_component, <<"Position">>),
  '7drl_match':move(Map, Player, X, Y, 0, -1),
  {ok, PlayerState#pstate{input=undefined}};

handle_input(PlayerState=#pstate{input=?DOWN_ARROW,
                                  pid=Player, map=Map}) ->
  {X, Y} = entity:call(Player, spatial_component, <<"Position">>),
  '7drl_match':move(Map, Player, X, Y, 0, 1),
  {ok, PlayerState#pstate{input=undefined}};

handle_input(PlayerState=#pstate{input=?LEFT_ARROW,
                                  pid=Player, map=Map}) ->
  {X, Y} = entity:call(Player, spatial_component, <<"Position">>),
  '7drl_match':move(Map, Player, X, Y, -1, 0),
  {ok, PlayerState#pstate{input=undefined}};

handle_input(PlayerState=#pstate{input=?RIGHT_ARROW,
                                  pid=Player, map=Map}) ->
  {X, Y} = entity:call(Player, spatial_component, <<"Position">>),
  '7drl_match':move(Map, Player, X, Y, 1, 0),
  {ok, PlayerState#pstate{input=undefined}};

handle_input(PlayerState=#pstate{input=[?LEFT_MOUSE, TargetX, TargetY],
                                  pid=Player, map=Map}) ->
  Current = entity:call(Player, spatial_component, <<"Position">>),
  Action = #action{type = <<"PrimaryAction">>, by = Player,
                    at = Current, to = {TargetX, TargetY}},
  entity:trigger(Map, Action),
  {ok, PlayerState#pstate{input=undefined}};

handle_input(PlayerState=#pstate{input=[?RIGHT_MOUSE, TargetX, TargetY],
                                  pid=Player, map=Map}) ->
  Current = entity:call(Player, spatial_component, <<"Position">>),
  Action = #action{type = <<"SecondaryAction">>, by = Player,
                    at = Current, to = {TargetX, TargetY}},
  entity:trigger(Map, Action),
  {ok, PlayerState#pstate{input=undefined}};

handle_input(PlayerState) -> {ok, PlayerState}.

position_to_key(X,Y) ->
  list_to_binary(integer_to_list(X) ++ "," ++ integer_to_list(Y)).

integer_to_binary(Int) ->
  list_to_binary(integer_to_list(Int)).

get_presentation(Player) ->
  Text = <<"symbol">>,
  [Text, Symbol] = case entity:call(Player, runemaster_component, <<"Rune">>) of
    undefined ->
      get_presentation(Player, {player, Player});
    RunePid when is_pid(RunePid) ->
      get_presentation(Player, {rune, RunePid})
  end,
  HP = integer_to_binary(entity:call(Player, health_component, <<"HP">>)),
  Damage = integer_to_binary(entity:call(Player, shotgun_component, <<"Damage">>)),
  [Text, <<Symbol/binary,",",HP/binary,",",Damage/binary>>].

get_presentation(_Player, {player, Pid}) ->
  [<<"symbol">>, <<"@">>];

get_presentation(Player, {wall, Pid}) ->
  [<<"symbol">>, <<"#">>];

get_presentation(Player, {rune, Pid}) ->
  Return = [<<"symbol">>, entity:call(Player,
    identify_component, {<<"Symbol">>, {rune, Pid}})],
  Return;

get_presentation(_Player, {effect, Pid}) ->
  % TODO: Another way to detect the type of effect
  Return = try entity:call(Pid, effect_component, <<"Effect">>) of
    Effect -> [<<"effect">>, Effect]
  catch
    Type:Msg ->
      io:format("~p error ~p~n", [Type,Msg]),
      [<<"error">>, <<"bug">>]
  end.

get_symbol(Entity, KnownSymbols) ->
  case dict:find(Entity, KnownSymbols) of
    {ok, Symbol} -> {Symbol, KnownSymbols};
    error ->
      {<<"@">>, dict:store(Entity, <<"@">>, KnownSymbols)}
  end.


